# Configuring and tuning the PVE hypervisor

## Configure the correct package repositories.
Make sure that the Proxmox VE repositories are configured correctly. Installing from the PVE 6.0 ISO image adds an entry to your APT sources list for the enterprise repositories. If you are not an enterprise customer, you need to remove that repository and add the free repos before upgrading.

Comment out the only line in `/etc/apt/sources.list.d/pve-enterprise.list` to remove the enterprise repos.

Edit `/etc/apt/sources.list` and add the "No-Subscription" repos by adding the following line:

>deb http://download.proxmox.com/debian/pve buster pve-no-subscription

Sync the new repos and upgrade the system:

>sudo apt update && sudo apt dist-upgrade


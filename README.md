# cloud-server-scripts

Scripts to install and configure Linux servers for different use cases. Install a base set of packages wanted for all systems, and provide additional scripts to automate initial setup of various services.
#!/usr/bin/env bash
#
# Generate server keys.
umask 077 
wg genkey | tee priv_srv_key | wg pubkey > pub_srv_key

# You can generate client keys here as well. You will need the client's public key to set up the interface.
wg genkey | tee priv_client_key | wg pubkey > pub_client_key

# Copy server config template to Wireguard directory.
# sudo cp ./conf.d/wg0.conf /etc/wireguard/wg0.conf

##
## Configure the server's config file using the server's private key and the client's public key.
##

# Set permissoins on the configs.
chown -v root:root /etc/wireguard/wg0.conf
chmod -v 600 /etc/wireguard/wg0.conf

# Bring up the interface.
wg-quick up wg0

# Enable the interface to persist after a reboot.
systemctl enable wg-quick@wg0.service
systemctl status wg-quick@wg0.service
echo 'Service is enabled and running...'
#!/usr/bin/env bash
#
# This script installs WireGuard kernel modules and userspace tools on Ubuntu 18.04.2+ LTS and 18.10 systems.
# Update package manifest and upgrade if needed.
apt-get update
apt-get upgrade -y

# Ensure dependencies are installed.
apt-get install -y \
    libmnl-dev \
    git \
    libelf-dev \
    linux-headers-$(uname -r) \
    build-essential \
    pkg-config \
    dkms \
    software-properties-common \
    binutils \
    libtool

# Add the WireGuard PPA.
add-apt-repository -y ppa:wireguard/wireguard

# Update package manifest to include the PPA we added.
apt-get update

# Install the 'wireguard' meta-package.
apt-get install -y wireguard

# Load the kernel module.
modprobe wireguard

# Enable kernel module at boot time.
echo 'wireguard' | tee -a /etc/modules
update-initramfs -u

# Enable IP forwarding.
# This is only necessary if this host is the wireguard server.
#sysctl -w net.ipv4.ip_forward=1
#sysctl -w net.ipv6.conf.all.forwarding=1
#
# Enable IP forwarding at boot time.
# This is only necessary if this host is the wireguard server.
#echo 'net.ipv4.ip_forward=1' | tee -a /etc/sysctl.conf
#echo 'net.ipv6.conf.all.forwarding=1' | sudo tee -a /etc/sysctl.conf
#update-initramfs -u

# List enabled modules:
lsmod | grep wireguard

# Print exit and suggest reboot.
echo 'WireGuard is installed. The kernel modules are enabled and the system has been configured to forward packets.....'
echo 'You can now generate keys and bring up interfaces....'
exit

#!/usr/bin/env bash
##
## Install script to speed up provisioning of minimal Ubuntu/Debian servers.
## Assumes Ubuntu version >18.04 or Debian Stretch version >9.4



## Do a full update of your Linux system.
## Sync the APT cache to the current repository.
apt-get update

## Upgrade all eligible packages:
apt-get -y dist-upgrade

## Remove any "orphaned" packages:
apt-get -y autoremove

## Clean the APT cache and remove old package versions:
apt-get autoclean

## Now, with a clean and tidy APT cache, we again sync the APT cache with the current package list:
apt-get update



## Install some essential packages.
apt-get -y install \
    haveged \
    htop \
    dialog \
    pkg-config \
    software-properties-common \
    curl \
    nano \
    rsync \
    openssh-server \
    file \
    libarchive-tools \
    p7zip-full \
    zip \
    unzip \
    unrar \
    less \
    tree \
    apt-transport-https \
    ca-certificates \
    gnupg-agent \
    libmnl-dev \
    unattended-upgrades



## Install the GNU C compiler and tools for building software:
apt-get -y install \
    build-essential \
    libtool \
    autoconf \
    git \
    dkms \
    binutils \
    cmake \
    gcc \
    linux-headers-$(uname -r)



## Install some basic command-line tools for information gathering:
apt-get -y install \
    nmap \
    net-tools \
    ethtool \
    lsof \
    iptraf-ng \
    iftop \
    iotop \
    iputils-*



## Install Red Hat system tuning daemon:
#apt-get -y install tuned
#
## Set profile to virtual-guest:
#tuned-adm profile virtual-guest
#
## Enable the service and then restart it for the new profile to take effect.
#systemctl enable tuned.service
#systemctl restart tuned.service



## Install Docker and its dependencies:
## Add Docker's official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
#
## Add Docker's official "stable" repository:
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#
## Update APT cache:
apt-get update
#
#
#
## Install the latest version of Docker CE, containerd, and docker-compose:
## Remember: unless you plan to run containers as root, you must add a user to the "docker" group.
## Kubernetes will run all containers as root.
apt-get -y install \
    docker-ce \
    docker-ce-cli \
    docker-compose \
    containerd.io



## Install Fail2Ban brute force mitigation:
apt-get -y install \
    fail2ban \
    iptables \
    python3-pyinotify

## Enable fail2ban service and create a static config file for future editing:
## It is highly recommended that you change the settings within this file to suit your deployment.
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

## Ensure ownership of new file:
chown root:root /etc/fail2ban/jail.local

## Ensure permissions of new file:
chmod 640 /etc/fail2ban/jail.local

## Enable fail2ban service:
## WARNING: By default, users who fail to authenticate 5 times over a 2 minute period will have their IP address banned for 10 minutes.
## Proceed with caution.
systemctl enable fail2ban.service
systemctl restart fail2ban.service
#
#
#
## That's it! Thanks for playing.
##
echo 'ALL DONE!'
exit

import requests
import os
import argparse

parser = argparse.ArgumentParser()

#-s START_DATE -e END_DATE -pid PRODUCT_ID -rid RATE_ID -sid SUPPLIER_ID
parser.add_argument("-s", "--start", dest = "START", help="Start Date")
parser.add_argument("-e", "--end", dest = "END", help="End Date")
parser.add_argument("-pid", "--product", dest = "PID", help="Product UUID")
parser.add_argument("-rid", "--rate", dest="RID", help="Rate UUID")
parser.add_argument("-sid", "--supplier", dest="SID", help="Supplier UUID")

args = parser.parse_args()

os.environ["API_KEY"] = "key-csecc-v8djx910vjcn85lcz8c7"
os.environ["API_SECRET"] = "secret-csecc-jf8c7h5n9v7cxf39z0fh"

url = "https://booking.sandbox.redeam.io/v1.1/suppliers/{os.environ["SID"]}/product/{os.environ["PID"]}/

querystring = {"start": os.environ["START"],"end": os.environ["END"]}

headers = {
    'X-API-Key': os.environ["API_KEY"],
    'X-API-Secret': os.environ["API_SECRET"],
    'User-Agent': "PostmanRuntime/7.15.0",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "7e9a93f2-c2c1-49c6-93a2-eaca2a76e633,b9358859-1d63-48da-8ebb-cda8062bec3e",
    'Host': "booking.sandbox.redeam.io",
    'accept-encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, headers=headers, params=querystring)


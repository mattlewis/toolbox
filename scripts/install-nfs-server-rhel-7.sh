#!/usr/bin/env bash
## Update yum repos.
sudo yum update

## Install rcpbind and nfs-utils packages.
sudo yum install -y rpcbind nfs-utils

## Make sure the NFS kernel module is loaded.
sudo modprobe nfs
sudo lsmod | grep nfs

## Create a directory where we will export our NFS share.
sudo mkdir /opt/nfs

## Edit the /etc/exports file and add an export line for each client machine.
## Eample which exports our '/opt/nfs' directory to two client machines:
/opt/nfs 10.5.10.2(no_root_squash,rw,sync)
/opt/nfs 10.10.10.5(no_root_squash,rw,sync)

## Start the rcpbind service, and then start the NFS server.
sudo systemctl enable --now rpcbind
sudo systemctl enable --now nfs

## Is the service up and running?
sudo systemctl status nfs

## Default installations of RHEL7 have firewalld installed and running.
## Allow clients to connect to the NFS server on TCP port 2049:
sudo firewall-cmd --zone=public --add-port=2049/tcp --permanent
sudo firewall-cmd --reload

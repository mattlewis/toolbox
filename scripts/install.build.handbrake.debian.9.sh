#!/usr/bin/env bash

# Install dependencies for Handbrake and the GTK grahical UI:
apt update
apt dist-upgrade -y
apt install  -y autoconf automake build-essential cmake git libass-dev libbz2-dev libfontconfig1-dev libfreetype6-dev libfribidi-dev libharfbuzz-dev libjansson-dev libmp3lame-dev libogg-dev libopus-dev libsamplerate-dev libtheora-dev libtool libvorbis-dev libx264-dev libxml2-dev m4 make patch pkg-config python tar yasm zlib1g-dev libtool-bin intltool libappindicator-dev libdbus-glib-1-dev libglib2.0-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgtk-3-dev libgudev-1.0-dev libnotify-dev libwebkit2gtk-4.0-dev

# Clone the Handbrake repos and enter the cloned directory:
git clone https://github.com/HandBrake/HandBrake.git && cd HandBrake

# Choose the current version for git to checkout:
git tag --list | grep ^1\.1\.
git checkout refs/tags/$(git tag -l | grep -E '^1\.1\.[0-9]+$' | tail -n 1)

# Configure the build files and compile for your system:
./configure --launch-jobs=$(nproc) --launch

# Install the compiled binaries to the system:
sudo make --directory=build install

#To start over, simply remove the build directory.
# sudo rm -rf build

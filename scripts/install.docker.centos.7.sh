#!/usr/bin/env bash
# Enable EPEL repos:
echo 'Adding EPEL Repository: https://fedoraproject.org/wiki/EPEL'
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Install dependencies:
echo 'Updating package manifest and installing dependencies....'
yum update
yum install -y yum-utils device-mapper-persistent-data lvm2

# Add Docker CE repo, update package manifest and install any new updates.
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum update
yum upgrade -y

# Install Docker CE.
yum install docker-ce docker-compose -y

# Enable Docker service and control socket.
systemctl enable --now docker.service
#systemctl enable --now docker.socket
echo 'Docker daemon and control socket are now running and enabled at boot....'

# Print exit, suggest adding user to 'docker' group and suggest a reboot.
echo 'Installation complete. Add your user to the group docker with:'
echo 'sudo usermod -aG docker YOU'
echo 'NOTE: You will need to login to a new shell or reboot for this to take effect....'
echo '-----  Installation Complete  -----'

#!/usr/bin/env bash
# Enable Red Hat Enterprise Linux Repos.
# Modify the next line to fit this syntax: "subscription-manager register --username $USERNAME --password $PASSWORD --auto-attach"
subscription-manager register --username mattlewis53 --password Corsica22 --auto-attach

# Enable 'supplementary' repos.
echo 'Enabling Red Hat Enterprise Linux supplementary repository...'
subscription-manager repos --enable rhel-7-server-supplementary-rpms

# Enable 'extras' repo.
echo 'Enabling Red Hat Enterprise Linux extras repository...'
subscription-manager repos --enable rhel-7-server-extras-rpms

# Enable 'optional' repo.
echo 'Enabling Red Hat Enterprise Linux optional repository...'
subscription-manager repos --enable rhel-7-server-optional-rpms

# Enable EPEL repos.
echo 'Enabling EPEL repository...'
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Update the package repos and install updates if necessary.
yum update -y

# Exit.
echo 'All done...'
#!/usr/bin/env bash

# This script will add Docker Community Edition repositories to Debian 9.x and then download, install, and enable the Docker service.
# https://hub.docker.com/explore
# [mattlewis53@gmail.com]

# Install dependencies:
echo "Installing Docker dependencies..."
apt update
apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common dkms aufs-tools -y

# Add the GPG key.
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -


# Add the repo.
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update package index and upgrade packages if necessary:
echo "Building new manifest and updating..."
apt-get update
apt-get upgrade -y

# Install the latest version of Docker CE:
echo "Installing Docker CE and docker-compose..."
apt-get install -y docker.io docker-compose python-docker

# Enable Docker service and control socket:
echo "Enabling Docker daemon..."
systemctl enable --now docker.service
systemctl enable --now docker.socket


# Load aufs modules if necessary:
#echo 'Loading the aufs module...'
#modprobe aufs


# Print status of Docker daemon:
systemctl status docker.service


# Print exit, suggest adding user to 'docker' group and suggest a reboot.
echo 'Installation complete. Add your user to the group docker with:'
echo 'sudo usermod -aG docker YOU'
echo '( You will need to relogin or reboot! )'

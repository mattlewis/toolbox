#!/usr/bin/env bash
#
# This is for systems based on Debian 9 'Stretch' or Ubuntu 16.04.5 LTS or newer.
# This script will do two things:
# - Install Netdata from stable git repo.
# - Enable auto-updating of Netdata once per day.
#
# Update package manifest:
echo 'Updating package manifest and installing dependencies....'
apt-get -y update
# Upgrade packages.
apt-get -y dist-upgrade

# Install Netdata dependencies.
apt-get -y install liblz4-dev \
                   autoconf-archive \
                   build-essential \
                   git \
                   pkg-config \
                   binutils \
                   libtool \
                   libuv1-dev \
                   libssl-dev \
                   libjudy-dev \
                   zlib1g-dev \
                   uuid-dev \
                   haveged \
                   libmnl-dev \
                   gcc \
                   make \
                   git \
                   autoconf \
                   autoconf-archive \
                   autogen \
                   automake \
                   pkg-config \
                   curl \
                   bash \
                   curl \
                   iproute2 \
                   python \
                   python-yaml \
                   python-ipaddress \
                   python-psycopg2 \
                   python-pymongo \
                   lm-sensors \
                   libmnl0 \
                   netcat

# Cleanup
echo 'Cleaning up...'

# Update package manifest:
apt-get update

# Remove dangling packages:
apt-get -y autoremove

# Clean the package cache:
apt-get autoclean

# ONE LINE INSTALLER FOR ALL SYSTEMS!
echo 'Installing Netdata. Press Enter when prompted....'
bash <(curl -Ss https://my-netdata.io/kickstart.sh)

# Enable KSM
echo 1 >/sys/kernel/mm/ksm/run
echo 1000 >/sys/kernel/mm/ksm/sleep_millisecs

# Exit
echo 'Installation complete. The server is now running on TCP port 19999...'
echo 'Have a nice day!'
exit

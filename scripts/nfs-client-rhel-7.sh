#!/usr/bin/env bash
## On the client machine make sure the correct NFS packages are installed.
sudo yum update
sudo yum install nfs-utils

## Create an empty directory where we will mount the NFS share.
sudo mkdir -p /mnt/nfs-share-1

## Mount the NFS share.
sudo mount -t nfs 192.168.1.20:/rpool/test-share /mnt/nfs-share-1

## Check to see if the share mounted with:
df -h

## Give current user and group ownership of the share directory.
## Left this commented out - be extra careful when recursively changing permissions on directories as root.
## sudo chown -R $USER:$GROUP /mnt/nfs-share-1

#!/usr/bin/env bash
rsync -avz -e "ssh -i /home/matty/.ssh/id_rsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" tom@10.13.13.13:/opt/ /mnt/external/backups/tom/opt/
rsync -avz /opt/ /mnt/external/backups/matty/opt/
rsync -avz /mnt/small/syncthing/ /mnt/external/backups/matty/syncthing/
rsync -avz /home/matty/ /mnt/external/backups/matty/home/
